CONTENTS OF THIS FILE
---------------------

 * Nice Node Add
 * Installation

Nice Node Add
----------------------

 * The Nice Node Add overrides node/add page. Allows users to add a icon to a content type and group content types.

Installation
----------------------

 * Install and enable module.
