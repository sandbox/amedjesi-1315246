<?php

/**
 * @file
 * Module file for Nice Node Add.
 */

/**
 * Implementation of hook_init().
 */
function nice_node_add_init() {
    drupal_add_css(drupal_get_path('module', 'nice_node_add') .'/nice_node_add.css', 'module');
}

/**
 * Implementation of hook_theme().
 */
function nice_node_add_theme() {
  return array(
    'nice_node_add_list' => array(
      'arguments' => array('content'),
      'file' => 'nice_node_add.pages.inc',
    ),
    'nice_node_add_item' => array(
      'arguments' => array('item', 'extra_class' => NULL),
      'file' => 'nice_node_add.pages.inc',
    ),
  );
}

/**
 * Implementation of hook_menu().
 *
 * Override core node/add page.
 */
function nice_node_add_menu_alter(&$items) {
  $items['node/add']['page callback'] = 'nice_node_add_page';
  $items['node/add']['file path'] = drupal_get_path('module', 'nice_node_add');
  $items['node/add']['file'] = 'nice_node_add.pages.inc';
}

/**
 * Implementation of hook_perm().
 */
function nice_node_add_perm() {
  $items = array();

  $node_types = node_get_types();

  foreach ($node_types as $node) {
    $items[] = 'show ' . $node->type;
  }

  return $items;
}

/**
 * Implementation of hook_form_alter().
 *
 * Add module settings to content type form.
 */
function nice_node_add_form_node_type_form_alter(&$form, $form_state) {
  $form['nice_node_add_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Nice Node Add'),
    '#description' => t('Settings for node/add page. To have an icon for this content type the filepath to the image should be %path.'
      , array('%path' => drupal_get_path('module', 'nice_node_add') .'/icons/' . $form['#node_type']->type .'.???')),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['nice_node_add_settings']['nice_node_add_settings_group'] = array(
    '#type' => 'textfield',
    '#title' => t('Group'),
    '#description' => t('The group this content type is in.'),
    '#default_value' => nice_node_add_get_setting($form['#node_type']->type),
    '#size' => 60,
    '#maxlength' => 128,
  );
}

/**
 * Implementation of preprocess_node().
 *
 * Add icon path to vars array.
 */
function nice_node_add_preprocess_node(&$vars) {
  $vars['icon_path'] = nice_node_add_get_icon_path($vars['type']);
}

/**
 * Get the nice node add setting associated with the given content type.
 */
function nice_node_add_get_setting($type) {
  return variable_get('nice_node_add_settings_group_'. $type, 'Other');
}



/**
 * Get the icon path for a content type.
 */
function nice_node_add_get_icon_path($type) {
  $base_icon_path = drupal_get_path('module', 'nice_node_add') .'/icons';
  $default_icon_path = $base_icon_path .'/default.png';
  $type_icon_path = $base_icon_path .'/' . $type;

  if (file_exists($type_icon_path)) {
    return $type_icon_path;
  }

  return $default_icon_path;
}