<?php

/**
 * @file
 * Page file for Nice Node Add module.
 */

/**
 * Create the list of content types.
 */
function nice_node_add_page() {
  $list = array();
  $node_types = node_get_types();

  foreach ($node_types as $node) {
    if (user_access('show ' . $node->type)) {
      $list[$node->type]['name'] = $node->name;
      $list[$node->type]['description'] = $node->description;
      $list[$node->type]['icon'] = nice_node_add_get_icon_path($node->type);
      $list[$node->type]['group'] = nice_node_add_get_setting($node->type);
      $list[$node->type]['href'] = node_access('create', $node->type)
        ? 'node/add/' . str_replace('_', '-', $node->type) : $list[$node->type]['href'] = '';
    }
  }

  return theme('nice_node_add_list', $list);
}

/**
 * Generate the HTML output for the node add list.
 */
function theme_nice_node_add_list($content) {
  // Sort the content by name.
  uasort($content, '_nice_node_add_sort_by_name');

  // Pull group information from the content list.
  $groups = array();
  foreach ($content as $item) {
    if (!isset($item['group']) || !$item['group']) {
      $item['group'] = 'Other';
    }
    $groups[$item['group']][$item['name']] = $item;
  }

  // Sort the groups.
  uksort($groups, _nice_node_add_sort_by_group);

  // Create a bunch of HTML.
  $output = '<div class="nice-node-add">';
  foreach ($groups as $group => $items) {
    $output .= '<h3>' . t($group) .'</h3>';
    $output .= '<ul class="nice-node-add-group">';
    $count = 0;
    foreach ($items as $key => $item) {
      if ($count % 2) {
        $extra_class = 'odd';
      }
      else {
        $extra_class = 'even';
      }
      $output .= theme('nice_node_add_item', $item, $extra_class);
      $count++;
    }
    $output .= '</ul>';

  }
  $output .= '</div>';

  return $output;
}

/**
 * Generate the HTML output for an node add item.
 */
function theme_nice_node_add_item($item, $extra_class = NULL) {
  $class = "nice-node-add-item";

  if (!empty($extra_class)) {
    $class .= ' '. $extra_class;
  }

  $output .= '<li class="' . $class .'">';
  if ($item['href'] != '') {
    $output .= l('<img class="nice-node-add-icon" src="'. $item['icon'] . '"></img>', $item['href'], array('html' => TRUE));
    $output .= '<div class="nice-node-add-info"><h4>'. l(t($item['name']),  $item['href']) .'</h4>' . t($item['description']) .'</div>';
  }
  else {
    $output .= '<img class="nice-node-add-icon" src="'. $item['icon'] . '"></img>';
    $output .= '<div class="nice-node-add-info"><h4>'. t($item['name']) .'</h4>' . t($item['description']) .'</div>';
  }
  $output .= '</li>';

  return $output;
}

/**
 * Array sorting callback; sorts by name.
 */
function _nice_node_add_sort_by_name($a, $b) {
  return strcasecmp($a['name'], $b['name']);
}

/**
 * Array sorting callback; sorts by group.
 *
 * A speical case if the group name is called "Other"; it should be placed at the bottom.
 */
function _nice_node_add_sort_by_group($a, $b) {
  if ($a == 'Other') {
    return 1;
  }
  elseif ($b == 'Other') {
    return -1;
  }
  else {
    return strcasecmp($a, $b);
  }
}